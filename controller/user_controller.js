exports.userController = function () {
    var user_data = require('./../model/user_data');
    return {
        newUser: function (name) {
            return new Promise(function (resolve, reject) {
                newUser = new user_data();
                newUser.username = name;
                newUser.save(function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            });
        },
        deleteUser: function (name) {
            return new Promise(function (resolve, reject) {
                user_data.remove({
                    username: name
                }, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                })
            });
        },
        getData: function (name) {
            return new Promise(function (resolve, reject) {
                user_data.findOne({
                    username: name
                }, function (err, res) {
                    if (err) {
                        reject(err);
                    } else if (res) {
                        resolve(res);
                    } else {
                        reject(res);
                    }
                });
            });
        },
        pushGeoData: function (name, lat, lng, x_co, y_co, z_co) {
            var geoData = {
                "lat": lat,
                "lng": lng,
                "x_co": x_co,
                "y_co": y_co,
                "z_co": z_co
            }
            return new Promise(function (resolve, reject) {
                user_data.findOneAndUpdate({
                    "username": name,
                }, {
                    $push: {
                        "geoData": geoData
                    }
                }, function (err, res) {
                    if (err) {
                        reject(err);
                    } else if (res) {
                        resolve(res);
                    } else {
                        reject(res);
                    }
                });
            });
        }
    }
}