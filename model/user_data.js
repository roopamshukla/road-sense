mongoose = require('mongoose');
var user = mongoose.Schema({
    username : {
        type : String,
        unique : true,
    },
    geoData : [{
        lat : Number,
        lng : Number,
        x_co: Number,
        y_co: Number,
        z_co: Number,
    }]
})

module.exports = mongoose.model('user', user);