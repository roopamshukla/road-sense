mongoose = require('mongoose');
var plot = mongoose.Schema({
    lat: Number,
    lng: Number,
    x_co: Number,
    y_co: Number,
    z_co: Number,
    quality: Number
})

module.exports = mongoose.model('plot', plot);