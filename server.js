var express = require('express');
var bodyParser = require('body-parser');
var user = require('./model/user_data');
var userController = require('./controller/user_controller')
var plotController = require('./controller/plot_controller')
var port = process.env.PORT || 9000;
var app = express();
var mongoose = require('mongoose');
mongoose.connect('mongodb://roopam:your_password@ds233895.mlab.com:33895/roadsense');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \Authorization');
    next();
});
var api = express.Router();
var admin = express.Router();
var map = express.Router();
var plot = express.Router();
app.use('/api', api);
api.post('/pushgeodata', function (req, res) {
    var userCtrl = userController.userController();
    username = req.query.name;
    lat = req.query.lat;
    lng = req.query.lng;
    x_co = req.query.x_co;
    y_co = req.query.y_co;
    z_co = req.query.z_co;
    userCtrl.pushGeoData(username, lat, lng, x_co, y_co, z_co).then(function (result) {
        res.status(200).send('success');
    }, function (err) {
        res.status(500).send('error:pushgeodata');
        console.log(err)
    });
});
api.get('/getdata', function (req, res) {
    var userCtrl = userController.userController();
    username = req.query.name;
    userCtrl.getData(username).then(function (result) {
        res.status(200).json(result);
    }, function (err) {
        res.status(500).send('error:getdata');
        console.log(err)
    });
});
app.use('/admin', admin);
admin.post('/adduser', function (req, res) {
    var userCtrl = userController.userController();
    username = req.query.name;
    userCtrl.newUser(username).then(function (result) {
        res.status(200).send('success');
    }, function (err) {
        res.status(500).send('error:adduser');
        console.log(err)
    });
});
admin.get('/deleteuser', function (req, res) {
    var userCtrl = userController.userController();
    username = req.query.name;
    userCtrl.deleteUser(username).then(function (result) {
        res.status(200).send('success');
    }, function (err) {
        res.status(500).send('error:deleteuser');
        console.log(err)
    });
});
app.use('/map', map);
map.get('/marker', function (req, res) {
    res.sendFile(__dirname + '/view/index.html');
});
app.use('/plot', plot);
plot.get('/', function (req, res) {
    var plotCtrl = plotController.plotController();
    plotCtrl.getPlot().then(function (result) {
        res.status(200).json(result);
    }, function (err) {
        res.status(500).send('error:getPlot');
        console.log(err)
    });
});
app.listen(port);
console.log(port);